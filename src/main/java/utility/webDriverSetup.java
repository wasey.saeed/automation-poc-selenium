package utility;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

import static config.configProperties.*;

public class webDriverSetup {

    public static WebDriver driver;

    public static WebDriver initInstance()
    {
        ChromeOptions op = new ChromeOptions();
        op.addArguments("start-fullscreen");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(op);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.get(baseUrl);
        return driver;

    }

    public static WebDriver getDriver() {
        if (driver != null) {
            return driver;
        }
        else
        {
            throw new IllegalStateException("Driver has not been initialized");
        }
    }

    public static void finishDriver()
    {
        if (driver != null)
        {
            driver.quit();
            driver = null;
        }
    }
}
