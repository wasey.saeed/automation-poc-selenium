package utility;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static utility.webDriverSetup.getDriver;

public class generalFunctions {

    public static void clearField(String path){
        WebElement element = getDriver().findElement(By.xpath(path));
        int input = element.getAttribute("value").length();

        for(int i=0; i<input ;i++){
            element.sendKeys(Keys.BACK_SPACE);
        }
    }

    public static void GetPageTitle()
    {

    }
}
