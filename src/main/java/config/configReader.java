package config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class configReader {

    Properties reader = new Properties();
    String filePath = "src/main/resources/configuration.properties";

    {
        try {
            InputStream input = new FileInputStream(filePath);
            reader.load(input);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getUrl() {
        return reader.getProperty("url");
    }
}
