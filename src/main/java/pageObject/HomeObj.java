package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static utility.webDriverSetup.getDriver;

public class HomeObj {

    public void ClickOnLink(String link)
    {
        getDriver().findElement(By.xpath("//li[text()='"+link+"' and @role='menuitem']")).click();
    }
}
