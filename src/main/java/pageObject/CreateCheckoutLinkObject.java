package pageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.util.ArrayList;

import static utility.generalFunctions.*;
import static utility.webDriverSetup.*;


public class CreateCheckoutLinkObject {

    public static String PageTitleHeading = "//p[@class='sc-AxirZ eOiGZY']";
    public static String fieldLocator = "//input[@name='amount']";

    public static String url;

    public static String CheckoutLinkTitle()
    {
        PageTitleHeading = getDriver().findElement(By.xpath(PageTitleHeading)).getText();
        return PageTitleHeading;
    }


    public void SelectCurrencyCode() {

        getDriver().findElement(By.xpath("//div[@name='currency']//span[@class='ant-select-selection-item']")).click();
        getDriver().findElement(By.xpath("//div[@label='HKD']")).click();
    }

    public void enterAmount(int amount) {
       clearField(fieldLocator);
       getDriver().findElement(By.xpath( "//input[@name='amount']")).sendKeys(String.valueOf(amount));
    }

    public void generateCheckoutLink() {
        getDriver().findElement(By.xpath("//p[contains(text(),'Generate link')]//ancestor::button[@type='button']")).click();
    }

    public void CopyWidgetLinkMethodByButton()
    {
        getDriver().findElement(By.xpath("//p[contains(text(),'Copy link')]//ancestor::button[@type='button']")).click();
    }

    public void CopyWidgetLinkMethodByUrl()
    {
       url = getDriver().findElement(By.xpath("//div[contains(text(),'https://checkout')]")).getText();
    }


    public void openWidget()
    {
        ((JavascriptExecutor)getDriver()).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
        getDriver().get(url);

    }
}
//p[@class="sc-AxirZ efbVLl" and text() ='Currency']