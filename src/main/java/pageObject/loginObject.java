package pageObject;

import org.openqa.selenium.By;
import static utility.generalFunctions.*;
import static utility.webDriverSetup.*;

public class loginObject {

    public static String emailLocator = "//input[@name='email']";
    public static String Password = "//input[@name='password']";
    public static String signInBtn = "//button[@type='submit']";

    public String errorLocator = " //p[@class='sc-AxirZ jaSYhn']";

    public String HomeLocator = "//p[text()='Home']";



    public void enterInputInEmail(String email)
    {
        clearField(emailLocator);
        getDriver().findElement(By.xpath(emailLocator)).sendKeys(email);
    }

    public void enterInputInPassword(String value)
    {
        clearField(Password);
        getDriver().findElement(By.xpath(Password)).sendKeys(value);
    }

    public void pressSignInButton()
    {
        getDriver().findElement(By.xpath(signInBtn)).click();
    }

    public String validateStatement() {
         String Statement =  getDriver().findElement(By.xpath(errorLocator)).getText();
        return Statement;
    }

    public String validateSuccessStatement() {
        String Statement =  getDriver().findElement(By.xpath(HomeLocator)).getText();
        return Statement;
    }


}
