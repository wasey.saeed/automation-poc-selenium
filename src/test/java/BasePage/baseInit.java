package BasePage;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static utility.webDriverSetup.*;

public class baseInit {

    @BeforeSuite
    public void beforeSuite()
    {
        initInstance();

    }


    @AfterSuite
    public void afterSuite()
    {
        finishDriver();
    }
}
