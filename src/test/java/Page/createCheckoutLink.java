package Page;

import Assertion.CommonAssertion;
import BasePage.baseInit;
import org.testng.annotations.Test;
import pageObject.CreateCheckoutLinkObject;
import pageObject.HomeObj;

import static pageObject.CreateCheckoutLinkObject.*;

public class createCheckoutLink extends baseInit {

    CreateCheckoutLinkObject checkout = new CreateCheckoutLinkObject();
    CommonAssertion commonAssertion = new CommonAssertion();
    HomeObj homeObj = new HomeObj();

    @Test
    public void CreateACheckOutLink()
    {
        homeObj.ClickOnLink("Create checkout link");
        commonAssertion.VerifyStringEqual(checkout.CheckoutLinkTitle(),"Create checkout link");
        checkout.SelectCurrencyCode();
        checkout.enterAmount(100);
        checkout.generateCheckoutLink();
        checkout.CopyWidgetLinkMethodByUrl();
        checkout.openWidget();

    }
}
