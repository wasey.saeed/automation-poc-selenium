package Page;


import Assertion.CommonAssertion;
import BasePage.baseInit;
import org.testng.annotations.Test;
import pageObject.loginObject;

public class login extends baseInit {

    loginObject logObj = new loginObject();
    CommonAssertion commonAssertion = new CommonAssertion();


    @Test
    public void Invalid_login()
    {
        logObj.enterInputInEmail("Test");
        logObj.enterInputInPassword("test123");
        logObj.pressSignInButton();
        commonAssertion.VerifyStringEqual(logObj.validateStatement(),"email must be a valid email");

    }

    @Test
    public void Successful_login()
    {
        logObj.enterInputInEmail("wasey.saeed@xanpool.com");
        logObj.enterInputInPassword("Kobe#24#81");
        logObj.pressSignInButton();
        commonAssertion.VerifyStringEqual(logObj.validateSuccessStatement(),"Home");

    }
}
