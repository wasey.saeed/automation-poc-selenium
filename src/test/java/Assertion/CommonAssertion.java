package Assertion;

import org.testng.Assert;

public class CommonAssertion {


    public void VerifyIntEqual(int actual, int expected, String comment) {

        Assert.assertEquals(actual, expected, comment);
    }

    public void VerifyStringEqual(String actual, String expected) {

        Assert.assertEquals(actual, expected);

    }

    public void VerifyStringNotEqual(String actual, String expected, String comment) {
        Assert.assertNotEquals(actual, expected, comment);

    }


}
